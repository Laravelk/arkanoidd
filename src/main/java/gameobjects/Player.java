package gameobjects;

import java.awt.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Player implements GameObject {

    private int playerHeight = 10;
    private int playerWidth = 100;
    private Point position;

    private static Logger logger = Logger.getLogger(Player.class.getName());

    public Player(int gameHeight) {
        int heightConst = 20;
        this.position = new Point(-500, gameHeight / 2  - playerHeight);
    }

    /*
    * @return bounty point
    * */
    public Point bounceVector(Rectangle hitbox) {
        logger.log(Level.INFO, "Start beunce vector in player");
        Point p = new Point(1, 1);
        final int widthConst = 2;
        final int heightConst = 3;
        final int decConst = 10;
        Rectangle hbT = new Rectangle(position.x - playerWidth / widthConst, position.y- playerHeight / widthConst,
                playerWidth, playerHeight / heightConst);
        Rectangle hbB = new Rectangle(position.x - playerWidth / widthConst,
                position.y + playerHeight / widthConst - playerHeight / heightConst,
                playerWidth, playerHeight / heightConst);
        Rectangle hbL = new Rectangle(position.x - playerWidth / widthConst,
                position.y - playerHeight / widthConst, playerWidth / decConst, playerHeight);
        Rectangle hbR = new Rectangle(position.x + playerWidth / widthConst - playerWidth / decConst,
                position.y - playerHeight / widthConst, playerWidth / decConst, playerHeight);
        if (hbT.intersects(hitbox) || hbB.intersects(hitbox)) {
            p.y = -1;
        }
        if (hbR.intersects(hitbox) || hbL.intersects(hitbox)) {
            p.x = -1;
        }
        return p;
    }

    public Point getPosition() {
        return position;
    }

    public void setPosition(Point position) {
        this.position = position;
    }

    public void setXPosition(int position) {
        this.position.x = position;
    }

    @Override
    public void render(Graphics graphics) {
        graphics.setColor(Color.lightGray);
        graphics.fillRect(position.x - playerWidth / 2, position.y - playerHeight / 2, playerWidth, playerHeight);
    }
}
