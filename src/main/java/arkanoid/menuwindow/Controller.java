package arkanoid.menuwindow;

import arkanoid.SoundDriver;
import arkanoid.game.GameController;
import arkanoid.game.settings.SettingsController;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.logging.Level;
import java.util.logging.Logger;

// main menu controller
public class Controller {
    private int tickrate = 30;

    private Logger logger = Logger.getLogger(Controller.class.getName());

    public Controller(SoundDriver soundDriver) {
        View view = new View();
        view.setVisible(true);

        SettingsController settingsController = new SettingsController(tickrate, soundDriver);

        view.getCreateNewGameButton().addActionListener(e -> {
            String loggerInfo = "Game started";
            logger.log(Level.INFO, loggerInfo);
            GameController controller = new GameController(settingsController);
        });

        view.getSettingButton().addActionListener(e -> settingsController.setVisible(true));

        view.getSettingButton().addActionListener(e -> {
            settingsController.getView().addWindowListener(new WindowListener() {
                @Override
                public void windowOpened(WindowEvent e) {

                }

                @Override
                public void windowClosing(WindowEvent e) {

                }

                @Override
                public void windowClosed(WindowEvent e) {
                    tickrate = settingsController.getTickrate();
                }

                @Override
                public void windowIconified(WindowEvent e) {

                }

                @Override
                public void windowDeiconified(WindowEvent e) {

                }

                @Override
                public void windowActivated(WindowEvent e) {

                }

                @Override
                public void windowDeactivated(WindowEvent e) {

                }
            });
        });

        //SoundDriver.playSound(music).join();
    }
}
