package arkanoid.game.settings;

import arkanoid.SoundDriver;

import javax.swing.*;

public class SettingsController {
    private int tickrate;

    private SettingsView view = new SettingsView();

    public SettingsController(int currentlyTickrate, SoundDriver soundDriver) {
        tickrate = currentlyTickrate;

        view.getVolumeSlider().addChangeListener(e -> {
            soundDriver.setVolume((float)((JSlider) e.getSource()).getValue() / 100);
        });

        view.getTickrateSlider().addChangeListener(e -> tickrate = ((JSlider) e.getSource()).getValue());

        view.getOkButton().addActionListener(e -> view.dispose());

        view.setVisible(false);
    }

    public void setVisible(boolean t) {
        view.setVisible(t);
    }

    public int getTickrate() {
        return tickrate;
    }

    public JFrame getView() {
        return view;
    }
}
