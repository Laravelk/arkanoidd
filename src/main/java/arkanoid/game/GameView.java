package arkanoid.game;

import gameobjects.Ball;
import gameobjects.Block;
import gameobjects.Player;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GameView  extends JPanel {
    private int windowWidth = 0;
    private int windowHeight = 0;
    private int rowsCount = 6;
    private int columnsCount = 10;
    private int tickrate = 30;
    private Ball ball;
    private Player player;
    private int liveCount = 3;
    private boolean isRunning = false;
    private boolean isPaused = false;
    private LinkedList<Block> blocks;
    private long lastUpdate;
    private boolean blockDestroyed = false;

    private boolean restartAfterDeath = true;
    private GameThread gameThread = null;

    private int countOfDestroyedBlocks = 0;
    final static private float toMilliseconds = 1000000.0f;

    Image backgroundTitle;

    private static Logger logger = Logger.getLogger(GameView.class.getName());
    private final Color[] rowColors = new Color[]{Color.gray, Color.red.darker(), Color.yellow.darker(),
            Color.blue.darker(), Color.pink, Color.green.darker()};

    public GameView(int width, int height, int tickrate) {
        this.windowWidth = width;
        this.windowHeight = height;
        this.tickrate = tickrate;

        setSize(new Dimension(width, height));

        String backgroundPath = "src/main/resources/background.png";
        backgroundTitle = new ImageIcon(backgroundPath).getImage();

        setVisible(true);
        resetGame();

        this.setFocusable(true);
    }

    private void resetGame() {
        liveCount = 3;
        logger.log(Level.INFO, "reset game");
        ball = new Ball(this);
        player = new Player(windowHeight);
        createBlocks(rowsCount, columnsCount);
    }

    private void createBlocks(final int rows, final int columns) {
        blocks = new LinkedList<>();
        final int gap = 10;
        final float w_step = (((float) windowWidth - gap) / columns) - gap;
        final float h_step = 30;

        for (int i = 0; i < columns; i++) {
            for (int j = 0; j < rows; j++) {
                Block block = new Block(new Point((int) (i * (w_step + gap) + gap) - windowWidth / 2,
                        (int) (j * (h_step + gap) + gap) - windowHeight / 2 + 3 * gap), 70, 30,
                        rowColors[j % rowColors.length]);
                blocks.add(block);
            }
        }
    }

    public void blockBroken() {
        countOfDestroyedBlocks++;
        blockDestroyed = true;
        int endGameCount = 100;
        if (endGameCount == countOfDestroyedBlocks) {
            quit();
        }
    }

    /*
    * запускает поток игры
    * */
    void run() {
        if (null != gameThread) {
            if (gameThread.isAlive()) {
                gameThread.interrupt();
            }
        }
        resetGame();
        gameThread = new GameThread(this);
        gameThread.start();
    }



    /*
    * на каждой итерации проверяет, закончилась игра или нет
    * */
    void tick() {
        double deltaTime = (System.nanoTime() - lastUpdate) / toMilliseconds;
        ball.tick(deltaTime, blocks, windowWidth, windowHeight, player);
        if (blocks.isEmpty()) {
            gameOver();
        }
        repaint();
    }


    /*
    * Мяч улетел за пределы игры
    * */
    public void ballWasLosted() {
        --liveCount;
        if (0 == liveCount) {
            gameOver();
        } else {
            ball = new Ball(this);
        }
    }


    /*
    * конец игры
    * */
    void gameOver() {
        quit();
    }

    /*
    * выход из игры
    * */
    void quit() {
        isRunning = false;
    }

    /*
    * снимаем игру с паузы или ставим на паузу
    * */
    void togglePause() {
        isPaused = !isPaused;
    }

    boolean isRunning() {
        return isRunning;
    }

    void setRunning(boolean status) {
        isRunning = status;
    }

    boolean isPaused() {
        return isPaused;
    }

    void setPaused(boolean status) {
        isPaused = status;
    }

    /*
    * @param newPosition - новая координата игрока.
    * */
    void playerMoved(int newPosition) {
        player.setXPosition(newPosition);
        repaint();
    }

    @Override
    public void paint(Graphics graphics) {
        super.paint(graphics);
        graphics.translate((getWidth() - windowWidth)/2, (getHeight() - windowHeight) / 2);

        int bgScale = 3;
        for (int x = 0; x < windowWidth; x+=backgroundTitle.getWidth(null)*bgScale) {
            for (int y = 0; y < windowHeight; y+=backgroundTitle.getHeight(null)*bgScale) {
                graphics.drawImage(backgroundTitle, x, y, backgroundTitle.getWidth(null)*bgScale, backgroundTitle.getHeight(null)*bgScale, null);
            }
        }

        graphics.translate(windowWidth / 2, windowHeight / 2);

        player.render(graphics);
        ball.render(graphics);

        for (Block block : blocks) {
            block.render(graphics);
        }

        graphics.setColor(Color.black);
        graphics.setFont(new Font("Arial", Font.BOLD, 30));
        String msg = "";
        if (!isRunning) msg = "Please insert a coin (press space)";
        else if (isPaused) msg = "Game Paused";
        FontMetrics fm = graphics.getFontMetrics();
        graphics.drawString(msg, -fm.stringWidth(msg)/2, fm.getHeight()/2);
        graphics.setFont(new Font("Arial", Font.BOLD, 18));
        graphics.setColor(Color.red);
        graphics.drawString("Count: " + String.valueOf(countOfDestroyedBlocks), -610, 340);
        graphics.drawString("Count of live: " + String.valueOf(liveCount), -610, 310);
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public int getTickrate() {
        return tickrate;
    }

    public void setTickrate(int tickrate) {
        this.tickrate = tickrate;
    }
}
