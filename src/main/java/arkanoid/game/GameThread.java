package arkanoid.game;

import java.util.logging.Level;
import java.util.logging.Logger;

public class GameThread extends Thread {
    GameView gameView;

    Logger logger = Logger.getLogger(GameThread.class.getName());

    GameThread(GameView view) {
        gameView = view;
    }

    @Override
    public void run() {
        gameView.setRunning(true);
        gameView.setPaused(false);
        gameView.setLastUpdate(System.nanoTime());

        while (gameView.isRunning()) {
            try {
                if (gameView.isPaused()) {
                    gameView.setLastUpdate(System.nanoTime());
                    Thread.sleep(1);
                } else {
                    gameView.tick();
                    gameView.setLastUpdate(System.nanoTime());
                    Thread.sleep((long)(1000.0 / gameView.getTickrate()));
                }
            } catch (InterruptedException e) {
                logger.log(Level.WARNING, "Interrupted", e);
                Thread.currentThread().interrupt();
            }
        }
    }
}
