package arkanoid;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

// универсальный класс для проигрывания музыки
// проигрывает ТОЛЬКО wav формат
public class SoundDriver implements AutoCloseable {
    private boolean released = false;
    private AudioInputStream stream = null;
    private Clip clip = null;
    private FloatControl volumeControl = null;
    private boolean playing = false;

    private static Logger logger = Logger.getLogger(SoundDriver.class.getName());
    private static String exception = "Exception: ";

    public SoundDriver(final File f) {
        try {
            stream = AudioSystem.getAudioInputStream(f);
            clip = AudioSystem.getClip();
            clip.open(stream);
            clip.addLineListener(new Listener());
            volumeControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
            released = true;
        } catch (IOException | UnsupportedAudioFileException | LineUnavailableException exc) {
            logger.log(Level.SEVERE, exception + exc);
            released = false;
            close();
        }
    }

    // возвращает результат загрузки звука
    public boolean isReleased() {
        return released;
    }

    // проигрывается ли музыка в данный момент
    public boolean isPlaying() {
        return playing;
    }

	/*
	  Запуск звука
	  breakOld определяет поведение, если звук уже играется
	  Если breakOld==true, о звук будет прерван и запущен заново
	*/
    public void play(boolean breakOld) {
        logger.log(Level.INFO, "start playing");
        if (released) {
            if (breakOld) {
                clip.stop();
                clip.setFramePosition(0);
                clip.start();
                playing = true;
            } else if (!isPlaying()) {
                clip.setFramePosition(0);
                clip.start();
                playing = true;
            }
        }
    }

    // То же самое, что и play(true)
    public void play() {
        play(true);
    }

    // Останавливает воспроизведение
    public void stop() {
        if (playing) {
            clip.stop();
        }
    }

    public void close() {
        if (clip != null)
            clip.close();

        if (stream != null)
            try {
                stream.close();
            } catch (IOException exc) {
                logger.log(Level.SEVERE, exception + exc);
                Thread.currentThread().interrupt();
            }
    }

	/*
	  Установка громкости
	  x долже быть в пределах от 0 до 1 (от самого тихого к самому громкому)
	*/
    public void setVolume(float x) {
        logger.log(Level.INFO, "Set Volume on: {0} ", + x);
        if (x < 0) x = 0;
        if (x > 1) x = 1;
        float min = volumeControl.getMinimum();
        float max = volumeControl.getMaximum();
        volumeControl.setValue((max-min)*x+min);
    }

    // Возвращает текущую громкость (число от 0 до 1)
    public float getVolume() {
        float v = volumeControl.getValue();
        float min = volumeControl.getMinimum();
        float max = volumeControl.getMaximum();
        return (v-min)/(max-min);
    }

    // Дожидается окончания проигрывания звука
    public void join() {
        if (!released) return;
        synchronized(clip) {
            try {
                while (playing)
                    clip.wait();
            } catch (InterruptedException exc) {
                logger.log(Level.SEVERE, exception + exc);
                Thread.currentThread().interrupt();
            }
        }
    }

    // Статический метод, для удобства
    public static SoundDriver playSound(String path) {
        logger.log(Level.INFO, "Play sound which position in path: {0} ", path);
        File f = new File(path);
        SoundDriver snd = new SoundDriver(f);
        snd.play();
        return snd;
    }

    private class Listener implements LineListener {
        public void update(LineEvent ev) {
            if (ev.getType() == LineEvent.Type.STOP) {
                playing = false;
            }
        }
    }
}