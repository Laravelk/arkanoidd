FROM adoptopenjdk/openjdk11:alpine-jre

RUN mkdir -p /pr_dir
COPY ./target/arkanoid-1.0-SNAPSHOT.jar /pr_dir/proj.jar

CMD java -jar /pr_dir/proj.jar
